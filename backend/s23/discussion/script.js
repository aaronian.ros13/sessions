// [SECTION] If-else statments
let number = 1;

if(number > 1){
	console.log("The number is greater than 1!");
} else if (number < 1){
	console.log("The number is less than 1!");
} else {
	console.log("None of the conditions were true :(")
}


// Falsey values
if(false){
	console.log("Falsey");
}

if(0){
	console.log("Falsey");
}

if(undefined){
	console.log("Falsey");
}

// Truthy values
if(true){
	console.log("Truthy");
}

if(1){
	console.log("Truthy");
}

if([]){
	console.log("Truthy");
}


// Ternary Operators
let result = (1 < 10) ? true : false;

// This is the if-else equivalent of the ternary operation above.
// if(1 < 10) {
// 	return true;
// } else {
// 	return false;
// }

// If there are multiple lines within the if-else block, it's better to use the regular if-else syntax as the ternary operator is only capable of handling one-liners.
if(5 == 5){
	let greeting = "hello";
	console.log(greeting);
}

console.log("Value returned from the ternary operator is " + result);


// [SECTION] Switch Statements
// Same functionality as if-else but different syntax. Not as commonly used as if-else.
let day = prompt("What day of the week is it today?").toLowerCase();

switch(day){
	case 'monday':
		console.log("The day today is monday!");
		break;
	case 'tuesday':
		console.log("The day today is tuesday!");
		break;
	case 'wednesday':
		console.log("The day today is wednesday!");
		break;
	case 'thursday':
		console.log("The day today is thursday!");
		break;
	case 'friday':
		console.log("The day today is friday!");
		break;
	default:
		console.log("Please input a valid day naman paareh");
		break;
}


// [SECTION] Try/Catch/Finally Statements
// Primarily used for debugging and handling errors
function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	} catch(error){
		console.log(error.message)
	} finally {
		alert("Intensity updates will show new alert!");
	}
}

showIntensityAlert(56);
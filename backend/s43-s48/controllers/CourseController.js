const Course= require("../models/Course.js");


module.exports.addCourse = (request_body) => {
    let new_course = new Course({
        name: request_body.name,
        description: request_body.description,
        price: request_body.price
    });

    return new_course.save().then((course, error) => {
        if (error) return false;

        return true
    }).catch(error => console.log(error));
}

module.exports.getAllCourse = (request, response) => {
    return Course.find({}).then(result => {
        return response.send(result);
    })
}

module.exports.getAllActiveCourses = (request, response) => {
    return Course.find({isActive: true}).then(result => {
        return response.send(result);
    })
}

module.exports.getCourse = (request, response) => {
    return Course.findById(request.params.id).then(result => {
        return response.send(result);
    })
}

module.exports.updateCourse = (request, response) => {
    let updated_course_details = {
        name : request.body.name,
        description: request.body.description,
        price: request.body.price
    };

    return Course.findByIdAndUpdate(request.params.id, updated_course_details).then((course, error) => {

        if(error){
            return response.send({
                message: error.message
            })
        }

        return response.send({
            message: 'Course has been updated successfully!'
        })
    })
}

module.exports.archiveCourse = (request, response) => {
    return Course.findByIdAndUpdate(
        request.params.id,
        { isActive: false }
    ).then((result, error) => {
        if (error) return response.send(false);

        return response.send(true);
    }).catch(error => console.error(error));
}

module.exports.activateCourse = (request, response) => {
    const courseId = request.params.courseId;

    Course.findByIdAndUpdate(courseId, { isActive: true }, { new: true }).then((course) => {
            if (!course) {
                return response.status(404).json({ message: 'Course not found' });
            }
            return response.json({ message: 'Course has been activated successfully!', course });
        })
        .catch((error) => {
            return response.status(500).json({ message: error.message });
        });
}

module.exports.searchCourses = (request, response) => {

    const courseName = request.body.courseName;

    return Course.find({name: {$regex: courseName, $options: 'i'}}).then((course) => {
        response.send(course)
    }). catch(error => response.send ({
        message: error.message
    }))
}





const User = require('../models/User.js');
const Course = require('../models/Course.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then((result, error) => {
		if(error){
			return {
				message:error.message
			}
		}
		if (result.length <= 0){
			return false;
		}
		return true;
	})
}

module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) =>{
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully registereda user!'
		};
	}).catch(error => console.log(error));
}

module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		if(result == null){
			return response.send({
				message: "The user isn't registered yet."
			})
		}

		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Your password is incorrect.'
			})
		}
	}).catch(error => response.send(error));
}

module.exports.getProfile = (request_body) => {
    return User.findOne({ _id: request_body.id}).then(result => {
        if (!result) return { message: "The user isn't registered yet"};   
        result.password = ""; 

        return result;
    }).catch(error => console.log(error))
}

module.exports.enroll = async (request, response) => {
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	let isUserUpdated = await User.findById(request.user.id).then(user => {
		let new_enrollment = {
			couresId: request.body.courseId,
		}

		user.enrollments.push(new_enrollment);

		return user.save().then(updated_user => true).catch(error => error.message);
	})
	// Send any error within 'isUserUpdated as a response'
	if(isUserUpdated !== true){
		return respone.send({message: isUserUpdated});
	}

	let isCourseUpdated = await Course.findById(request.body.courseId).then(course => {
		let new_enrollee = {
			userId: request.user.id
		}

		course.enrollees.push(new_enrollee);
		return course.save().then(updated_course => true).catch(error => error.message)
	})

	if(isCourseUpdated !== true){
		return response.send({message: isCourseUpdated});
	}

	// [SECTION] Once isUserUpdated AND isCourseUpdated return true
	if(isUserUpdated && isCourseUpdated){
		return response.send({message: 'Enrolled Successfully!'});
	}
}

module.exports.getEnrollments = (request, response) => {
	User.findById(request.user.id)
		.then(result => response.send(result.enrollments))
		.catch(error => response.send(error.message))
}
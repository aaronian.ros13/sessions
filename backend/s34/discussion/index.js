db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"contact": {
	    "phone": "0912345678",
    	"email": "janedoe@gmail.com"
	},
	"courses": ["CSS", "Javascript", "Python"],
	"department": "none"
});

//

db.users.insertMany([
	{
		"firstName": "Jhon",
		"lastName": "Doe"
	},
	{
		"firstName": "Joseph",
		"lastName": "Doe"
	}
]);

//[SECTION] Retrieving Documents
//Retrieving all the inserted users
db.users.find();

//Retrieving a specific document form a collection
db.users.find({ "firstName": "Jhon" });

//[Section] Updating existing documents
db.users.updateOne(
	{
		"_id": ObjectId("64c1c68b4bc546bf24e70e35")
	},
	{
		$set: {
			"lastName":"Gaza"
		}
	}
);

db.users.updateOne(
    {
        "_id": ObjectId("64c1c45f98a5baa752634263") 
    },
    {
        $set: {
            "lastName": "Gaza"
        }
    }
);

db.users.updateMany(
	{
		"lastName":"Doe"
	},
	{
		$set: {
			"firstName":"Mary"
		}
	}
);



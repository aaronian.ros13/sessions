console.log("ES6 Updates")

// Exponent Operator
const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template Literals
/*
	- allows us to write strings w/o using concantenation operator (+)
	- greatly helps us with code readability
*/
let name = "Ken";

// using concatenation
// uses single quotes ('')
let message = 'Hello ' + name + '! Welcome to Programming';
console.log('Message without template literals: ' + message);

// using template literals
// uses backticks (``) and ${} for including javascript expressions
message = `Hello ${name}! Welcome to Programming`;
console.log(`Meesage with template literals: ${message}`);

// create multi-line statements using template literals
const anotherMessage = `
${name} attended a Math Competition.
He won it by solving the problem 8 ** 2 with the answer of ${firstNum}`;
console.log(anotherMessage);

/*
	- Template literals allow us to write strings with embedded JavaScript expressions
	- expressions are any valid unit of code that resolves to a value
	- "${}" are used to include JavaScript expressions in strings using template literals
*/
const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings amount is: ${principal * interestRate}`);

// Array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability
	- Syntax
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Juan", "Dela", "Cruz"];

// using array indeces
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

// using array destructuring
/*
const firstName = fullName[0];
const middleName = fullName[1];
const lastName = fullName[2];
*/
const [firstName, middleName, lastName] = fullName;

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

// Object Destructuring
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
	- Syntax
		let/const {propertyName, propertyName, propertyName} = object;
*/
const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
}

// using dot notation
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`);

// using object destructuring
const {givenName, familyName, maidenName} = person;

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you.`);

// using object destructuring in functions
function getFullName({givenName, maidenName, familyName}) {

	console.log(`${givenName} ${maidenName} ${familyName}`)
};

getFullName(person);

// Arrow Functions
/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
*/

const hello = () => {
	console.log('Hello World!')
}

hello();

// Traditional Functions without Template Literals
/*
function printFullName (fName, mName, lName) {
	console.log(fName + ' ' + mName + ' ' + lName);
}

printFullName('John', 'D', 'Smith');
*/

// Arrow Function with Template Literals
const printFullName = (fName, mName, lName) => {
	console.log(`${fName} ${mName} ${lName}`)
}

printFullName('Jane', 'D', 'Smith');

// Arrow Functions with Loops
const students = ['John', 'Jane', 'Judy']

// Traditional Function
students.forEach(function(student) {
	console.log(`${student} is a student.`)
})

// Arrow Function
// The function is only used in the "forEach" method to print out a text with the student's names
students.forEach((student) => {
	console.log(`${student} is a student.`)
})

// Implicit Return Statements
/*
	- There are instances when you can omit the "return" statement
	- This works because even without the "return" statement JavaScript implicitly adds it for the result of the function
*/

// Traditional Function
/*
function add(x, y) {
	let result = x + y;
	return result;
}

let total = add(2, 5);
console.log(total);
*/

// Arrow Function
// single line arrow functions
const add = (x, y) => x + y;

let total = add(2, 5);
console.log(total);

// Default Function Argument Values
// Provides a default argument value if none is provided when the function is invoked

const greet = (name = 'User') => {
	return `Good Afternoon ${name}`
}

console.log(greet()); // Good Afternoon User
console.log(greet('Judy')); // Good Afternoon Judy

// [SECTION] Class-based Object Blueprints

// Create a class
class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instantiate an object
const fordExplorer = new Car();

// Even though the 'fordExplorer' object is const, since it is an object, you may still re-assign values to its properties.
fordExplorer.brand = "Ford";
fordExplorer.name = "Explorer";
fordExplorer.year = 2022;

console.log(fordExplorer);

// This logic applies wether you re-assign the values of each property separately or put it as arguments of the new instance of the class.
const toyotaVios = new Car("Toyota", "Vios", 2018);

console.log(toyotaVios);
let http = require('http');

const port = 4000;

// Mock Database
let users = [
	{
		"name": "Paolo",
		"email": "paolo@email.com"
	},
	{
		"name": "Shinji",
		"email": "shinji@email.com"
	}
]

const app = http.createServer((req, res) => {

	// /items GET route
	if(req.url == '/items' && req.method == 'GET') {

		res.writeHead(200, {'Content-Type' : 'text/plain'});

		res.end('Data retrived from the database')
	}

	// /items POST route
	if(req.url == '/items' && req.method == 'POST') {
		res.writeHead(200, {'Content-Type' : 'text/plain'});

		res.end('Data sent to the data base!');
	}

	// getting all items from mock database
	if(req.url == '/users' && req.method == 'GET'){
		res.writeHead(200, {'Content-Type':'application/json'})

		res.end(JSON.stringify(users));
	}

	// creating a user in our database
	if(req.url == '/users' && req.method == 'POST'){
		// placeholder fot the data that is within the request
		let request_body = '';

		req.on('data', (data) => {
			request_body += data;
		})

		// the 'on' function listens fot specific behavior within the request. in this example, we are checking for when the request detects data within it. once the request recieves data, it will run a function
		req.on('end', () => {
			console.log(typeof request_body);

			request_body = JSON.parse(request_body);

			let new_user = {
				"name": request_body.name,
				"email": request_body.email
			}

			users.push(new_user);

			res.end(JSON.stringify(new_user));
		})
	}

})

app.listen(port, () => console.log(`Server is running at the localhost:${port}`));
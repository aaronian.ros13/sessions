// use 'require' directive to load the Node.js modules
// a module is a software component or part of a program that contains one or more routines
//the "http" module let Node.js transfer data using the HYPER TEXT TRANSFER PROTOCOL
// HTTP is a protocol that allows the fetching of resources like html documents.
let http = require("http");

// createServer() method creates a HTTP server that listens to request on a specific port and gives response to the client
// 4000 is the port where the server will listen to
http.createServer(function(request, response) {

	response.writeHead(200, {'Content-Type': 'text/plain'});

	response.end('Hello World');

}).listen(4000)

console.log('Server is running at 4000')
const express = require('express');
const router = express.Router();
const TaskController = require('../controllers/TaskController.js');

//Insert routes here
// [SECTION] Routes
// Creating a new task
router.post('/', (request, response) => {
		TaskController.createTask(request.body).then(result => {
			response.send(result)
		})
})

router.get('/', (request, response) =>{
	TaskController.getAllTasks().then(result => {
		response.send(result)
	})
})

module.exports = router;

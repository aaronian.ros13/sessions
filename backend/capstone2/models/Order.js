const mongoose = require('mongoose');

const order_schema = new mongoose.Schema({
    userId: [
        {
            user: {
                type: String,
                required: [true, "User is required"]
            }
        }
    ],
    products: [
        {
            productId: {
                type: String,
                required: [true, "productId is required"]
            },
            quantity: {
                type: Number,
                required: [true, "quantity is required"]
            }
        }
    ],
    totalAmount: {
        type: Number,
        required: [true, "totalAmount is required"]
    },
    purchasedOn: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("Order", order_schema);
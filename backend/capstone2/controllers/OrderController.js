const Order = require('../models/Order.js'); 
const Product = require('../models/Product.js');
const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.createOrder = async (request, response) => {
    try {
        // Check if the user is an admin
        if (request.user.isAdmin) {
            return response.send('Action Forbidden');
        }

        const user = await User.findById(request.user.id); // Find the user by their ID
        const products = request.body.products;

        const orderedProducts = await Product.find({ _id: { $in: products.map(p => p.productId) } });

        const totalAmount = orderedProducts.reduce((total, product) => {
            const orderedProduct = products.find(p => p.productId === product._id.toString());
            return total + orderedProduct.quantity * product.price;
        }, 0);

        const newOrder = new Order({
            userId: [{ user: user._id }],
            products: products.map(p => ({ productId: p.productId, quantity: p.quantity })),
            totalAmount
        });

        await newOrder.save();

        return response.send({ message: 'Order created successfully!' });
    } catch (error) {
        return response.status(500).send({ message: error.message });
    }
};

const Product= require("../models/Product.js");

module.exports.getAllProduct = (request, response) => {
    return Product.find({}).then(result => {
        return response.send(result);
    })
}

module.exports.addProduct = (request_body) => {
    let new_product = new Product({
        name: request_body.name,
        description: request_body.description,
        price: request_body.price
    });

    return new_product.save().then((course, error) => {
        if (error) return false;
    return {
			message: 'Successfully created Product!'
		};

        return true
    }).catch(error => console.log(error));
}

module.exports.getAllActiveProducts = (request, response) => {
    return Product.find({isActive: true}).then(result => {
        return response.send(result);
    })
}

module.exports.getProduct = (request, response) => {
    return Product.findById(request.params.id).then(result => {
        return response.send(result);
    })
}

module.exports.updateProduct = (request, response) => {
    let updated_product_details = {
        name : request.body.name,
        description: request.body.description,
        price: request.body.price
    };

    return Product.findByIdAndUpdate(request.params.id, updated_product_details).then((product, error) => {

        if(error){
            return response.send({
                message: error.message
            })
        }

        return response.send({
            message: 'product has been updated successfully!'
        })
    })
}

module.exports.archiveProduct = (request, response) => {
    const id = request.params.id;

    Product.findByIdAndUpdate(id, { isActive: false }, { new: true }).then((product) => {
        if (!product) {
            return response.status(404).json({ message: 'Product not found' });
        }
        return response.json({ message: 'Product has been archived successfully!', product });
    })
    .catch((error) => {
        return response.status(500).json({ message: error.message });
    });
}

module.exports.activateProduct = (request, response) => {
    const id = request.params.id;

    Product.findByIdAndUpdate(id, { isActive: true }, { new: true }).then((product) => {
            if (!product) {
                return response.status(404).json({ message: 'Product not found' });
            }
            return response.json({ message: 'Product has been activated successfully!', product });
        })
        .catch((error) => {
            return response.status(500).json({ message: error.message });
        });
}
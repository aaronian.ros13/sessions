const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


//Find All user
module.exports.getAllUser = (request, response) => {
    return User.find({}).then(result => {
        return response.send(result);
    })
}

//User registration
module.exports.registerUser = (request_body) => {
	let new_user = new User({
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) =>{
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully registereda user!'
		};
	}).catch(error => console.log(error));
}

//User authentication
module.exports.authenticateUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		if(result == null){
			return response.send({
				message: "The user isn't registered yet."
			})
		}

		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Your password is incorrect.'
			})
		}
	}).catch(error => response.send(error));
}

module.exports.getUser = (request_body) => {
    return User.findOne({ _id: request_body.id}).then(result => {
        if (!result) return { message: "The user isn't registered yet"};   
        result.password = ""; 

        return result;
    }).catch(error => console.log(error))
}








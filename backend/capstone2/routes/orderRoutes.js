const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/orderController'); 
const auth = require('../auth');

router.post('/create', auth.verify, (request, response) => {
	OrderController.createOrder(request, response);
}) 
	
module.exports = router;


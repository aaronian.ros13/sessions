const express = require("express");
const router = express.Router();
const ProductController = require("../controllers/ProductController.js");
const { verify, verifyAdmin } = require('../auth');

router.get('/all', (request, response) => {
    ProductController.getAllProduct(request, response);
})


router.post("/", verify, verifyAdmin, (request, response) => {
    ProductController.addProduct(request.body).then(result => {
        response.send(result)
    })
})

router.get('/', (request, response) => {
    ProductController.getAllActiveProducts(request, response);
})

router.get('/:id', (request, response) => {
    ProductController.getProduct(request, response);
})

router.put('/:id', verify, verifyAdmin, (request, response)=> {
    ProductController.updateProduct(request, response);
})

router.put('/:id/archive', verify, verifyAdmin, (request, response)=> {
    ProductController.archiveProduct(request, response);
})

router.put("/:id/activate", verify, verifyAdmin, (request, response) => {
    ProductController.activateProduct(request, response);
})

module.exports = router;

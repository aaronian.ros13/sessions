import { Card, Button} from "react-bootstrap";
import PorpTypes from 'prop-types';
import { useState } from 'react';

export default function CourseCard({course}) {

    const {name, description, price} = course;

    const [count, setCount] = useState(0);
    const [seat, seatCount] = useState(30);
    function enroll(){
        if (seat > 0){
            setCount(prev_value => prev_value + 1);
            seatCount(prev_value => prev_value - 1);
        } 
        else {
            alert("No more seats available.")
        }

    }

    return (
        <Card id="courseComponent1">
            
            <Card.Body>
                <Card.Text>
                <Card.Title>{name}</Card.Title>
                    <p>Description</p>
                    <p>{description}</p>
                </Card.Text>
                <Card.Text className="my-3">
                    <p>Price:</p>
                    <p>PHP{price}</p>

                    <Card.Subtitle>Enrollees: </Card.Subtitle>
                    <Card.Text>{count}</Card.Text>

                    <Card.Subtitle>Seats: </Card.Subtitle>
                    <Card.Text>{seat}</Card.Text>

                    <Button variant="primary" onClick={enroll}>Enroll</Button>
                </Card.Text>
            </Card.Body>


        </Card>
    );
}
CourseCard.PorpTypes = {
    course: PorpTypes.shape({
        name: PorpTypes.string.isRequired,
        description: PorpTypes.string.isRequired,
        price: PorpTypes.number.isRequired
    })
}
import {Button, Row, Col} from 'react-bootstrap';

export default function Banner(){
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Fishing for everyone!</p>
				<Button variant="primary">Enroll now!</Button>
			</Col>
		</Row>
	)
}
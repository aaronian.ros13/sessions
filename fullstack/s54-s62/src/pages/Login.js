import { useState, useEffect } from 'react';
import { Button, Form } from 'react-bootstrap';

export default function Login() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {

      if (email !== "" && password !== "") {
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    }, [email, password]);

    const loginUser = (event) => {
      event.preventDefault();

      fetch("http://localhost:4000/api/users/login", {
          method: "POST",
          headers: {
              "Content-Type": "application/json"
          },
          body: JSON.stringify({
              email: email,
              password: password
          })
      }).then(response => response.json()).then(result => {
        console.log(result);
        if(result) {
            setEmail("");
            setPassword("");                

            alert("Thankyou for logging in.")
        } else {
            alert("Unsuccessful Login")
        }
          
      });
  };

    return (
        <Form onSubmit={ event => loginUser(event) }>
        <h1 className="my-5 text-center">Login</h1>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email"
           placeholder="Enter email" 
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>
      
        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" 
          placeholder="Password" 
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
        </Form.Group>
        <Button variant="primary" type="submit" disabled={isActive === false}>
          Submit
        </Button>
      </Form>
    )
}